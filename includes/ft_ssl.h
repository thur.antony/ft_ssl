/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 16:55:05 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/08 16:55:07 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H

# include "../libftprintf/includes/ft_printf.h"
# include "ft_ssl_md5.h"
# include "ft_ssl_sha256.h"
# include <fcntl.h>
# include <dirent.h>

# define ERR_CODES			4
# define BAD_ALGO_OPT		0
# define BAD_FILE			1
# define DIR				2
# define BAD_MALLOC			3

# define ALG				3
# define MD5				1
# define SHA256				2

# define SSL_OPT_P			0x800
# define SSL_OPT_Q			0x400
# define SSL_OPT_R			0x200

# define SSL_D_CNT			0x8000
# define SSL_D_OUT			0x4000

typedef struct s_ssl_obj	t_ssl_obj;

struct						s_ssl_obj
{
	unsigned int			bitmap;
	int						i_algo;
	int						i_arg_max;
	int						content_length;
	int						stdin;
	char					**argv;
	char					*content;
	char					*file_name;
	char					*algo_name;
	char					*output;
	void					(*arg_process[ALG])(t_ssl_obj *o, int *i);
	void					(*algorithm[ALG])(t_ssl_obj *o);
	void					(*error[ERR_CODES])(t_ssl_obj *o);
};

void						ssl_obj_init(t_ssl_obj *o, int argc, char **argv);
void						ssl_obj_arg_process(t_ssl_obj *o);
void						ssl_obj_dispatcher(t_ssl_obj *o);
void						ssl_obj_error(t_ssl_obj *o);
void						ssl_enable_algo(t_ssl_obj *o, char *s);

void						arg_process_hash_module(t_ssl_obj *o, int *i);

void						process_string(t_ssl_obj *o, int i);
void						process_stdin(t_ssl_obj *o);
void						process_file(t_ssl_obj *o, int i);
void						interactive_mode_on(t_ssl_obj *o);

void						algo_md5(t_ssl_obj *o);
void						algo_sha256(t_ssl_obj *o);

void						err_zero(t_ssl_obj *o);
void						err_one(t_ssl_obj *o);
void						err_two(t_ssl_obj *o);
void						err_tree(t_ssl_obj *o);
void						usage(void);

void						sha256_format_output(t_sha256 *sha, t_ssl_obj *o);

#endif
