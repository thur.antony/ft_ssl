/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_md5.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 17:19:22 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/14 17:19:25 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_MD5_H
# define FT_SSL_MD5_H

# define ROT_L(x, c)	(((x) << (c)) | ((x) >> (32 - (c))))

typedef unsigned int		t_32;
typedef unsigned char		t_8;

typedef struct				s_md5
{
	t_32					h[4];
	t_8						*msg;
	t_32					len;
	t_32					offset;
	t_32					a;
	t_32					b;
	t_32					c;
	t_32					d;
	t_32					*word;
	t_32					temp;
	t_32					i;
	t_32					f;
	t_32					j;
	t_32					g;
}							t_md5;

#endif
