/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_sha256.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/16 12:38:10 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/16 12:38:12 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_SHA256_H
# define FT_SSL_SHA256_H

# define ASSIGN_SHA256		1
# define INCRM_SHA256		2

# define ROTL(x, n)			((x << n) | (x >> (32 - n)))
# define ROTR(x, n)			((x >> n) | (x << (32 - n)))
# define SHR(x, n)			(x >> n)

# define CH(x, y, z)		((x & y) ^ (~x & z))
# define MAJ(x, y, z)		((x & y) ^ (x & z) ^ (y & z))
# define SIGMA_0(x)			(ROTR(x, 2) ^ ROTR(x, 13) ^ ROTR(x, 22))
# define SIGMA_1(x)			(ROTR(x, 6) ^ ROTR(x, 11) ^ ROTR(x, 25))
# define SSM_0(x)			(ROTR(x, 7) ^ ROTR(x, 18) ^ SHR(x, 3))
# define SSM_1(x)			(ROTR(x, 17) ^ ROTR(x, 19) ^ SHR(x, 10))

typedef struct				s_sha256
{
	t_32					m[64];
	t_32					hh[8];
	t_8						*msg;
	t_32					len;
	t_32					offset;
	t_32					a;
	t_32					b;
	t_32					c;
	t_32					d;
	t_32					e;
	t_32					f;
	t_32					g;
	t_32					h;
	t_32					*word;
	t_32					t1;
	t_32					t2;
}							t_sha256;

void						sha256_vals(t_sha256 *sha, int opt);
void						sha256_shedule_arr(t_sha256 *sha);
void						sha256_inner_loop(t_sha256 *sha);
void						sha256_add_precision(char **tmp);

#endif
