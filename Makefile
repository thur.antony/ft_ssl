NAME = ft_ssl
LIB_NAME = ./libftprintf/libftprintf.a

SRC_PROJECT = ./srcs/main.c \
			./srcs/ssl_obj.c \
			./srcs/ssl_obj_content_out.c \
			./srcs/arg_process_module.c \
			./srcs/error_module.c \
			./srcs/algo_md5.c \
			./srcs/algo_sha256.c \
			./srcs/algo_sha256_hlp.c

OBJECT_PROJECT = $(SRC_PROJECT:.c=.o)
FLAGS = -O3 -Wall -Wextra -Werror -I./includes -I./libftprintf/includes
FLAGS_LIBFTPRINTF = -L./libftprintf -lftprintf

all: $(NAME)
	@echo '👾 compiling a project "ft_ssl" for iMac'
	@gcc -o $(NAME) $(FLAGS) $(SRC_PROJECT) $(FLAGS_LIBFTPRINTF)
	@echo '👾 ready!'

$(NAME): $(LIB_NAME) $(OBJECT_PROJECT)
	
$(LIB_NAME):
	@echo '- recompiling an cleaning library libftprintf.a -'
	@make -C ./libftprintf/ re
	@make -C ./libftprintf/ clean	

%.o: %.c
	@echo '👾 updating changes in .c files'
	@gcc $(FLAGS) -o $@ -c $<

clean:
	@echo '👾 cleaning a project'
	@/bin/rm -f $(OBJECT_PROJECT)

fclean: clean
	@echo '👾 removing a project'
	@/bin/rm -f $(NAME)
	@echo '- removing an cleaning library libftprintf.a -'
	@make -C ./libftprintf/ fclean

re:	fclean all