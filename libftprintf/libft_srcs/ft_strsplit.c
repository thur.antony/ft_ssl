/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/29 15:18:07 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 13:46:34 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_words_count(char const *s, char c)
{
	int		count;
	size_t	i;

	count = 0;
	i = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		if (s[i] != c && s[i] != '\0')
			count++;
		while (s[i] != c && s[i] != '\0')
			i++;
	}
	return (count);
}

static size_t	ft_word_len(char const *s, char c)
{
	size_t	i;
	size_t	len;

	i = 0;
	len = 0;
	while (s[i] == c)
		++i;
	while (s[i] != c && s[i] != '\0')
	{
		++len;
		++i;
	}
	return (len);
}

char			**ft_strsplit(char const *s, char c)
{
	int		w;
	size_t	j;
	size_t	i;
	int		w_count;
	char	**ar;

	if (!s || !(ar = (char **)malloc(sizeof(char *) *
					((w_count = ft_words_count(s, c)) + 1))))
		return (NULL);
	w = -1;
	j = 0;
	while (++w < w_count)
	{
		i = 0;
		if (!(ar[w] = ft_strnew(ft_word_len(&s[j], c) + 1)))
			ar[w] = NULL;
		while (s[j] == c)
			j++;
		while (s[j] != c && s[j] != '\0')
			ar[w][i++] = s[j++];
		ar[w][i] = '\0';
	}
	ar[w] = 0;
	return (ar);
}
