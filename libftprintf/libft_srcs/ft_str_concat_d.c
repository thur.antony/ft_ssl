/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_concat_d.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/09 14:04:59 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/09 14:05:01 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_str_concat_d(char *s1, int d1, char *s2, int d2)
{
	char	*res;
	int		size[3];
	int		i;

	size[1] = ft_strlen(s1);
	size[2] = ft_strlen(s2);
	size[0] = size[1] + size[2];
	res = ft_strnew(size[0]);
	i = 0;
	while (i < size[1])
	{
		res[i] = s1[i];
		i++;
	}
	while (i < size[0])
	{
		res[i] = s2[i - size[1]];
		i++;
	}
	if (d1)
		free(s1);
	if (d2)
		free(s2);
	return (res);
}
