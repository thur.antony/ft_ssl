/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_x.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:17:59 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:18:00 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					x_type(t_arg *o, va_list *ap)
{
	if (o->bitmap & HH)
		o->container.u = (unsigned char)va_arg(*ap, unsigned int);
	else if (o->bitmap & H)
		o->container.u = (unsigned short)va_arg(*ap, unsigned int);
	else if (o->bitmap & L || o->bitmap & J)
		o->container.u = va_arg(*ap, unsigned long);
	else if (o->bitmap & LL)
		o->container.u = va_arg(*ap, unsigned long long);
	else if (o->bitmap & Z)
		o->container.u = va_arg(*ap, size_t);
	else
		o->container.u = va_arg(*ap, unsigned int);
	o->dynamic_content = 1;
	if (o->bitmap & X_UP_CHECK)
		o->content = ft_itoa_base_u_up(o->container.u, 16);
	else
		o->content = ft_itoa_base_u_low(o->container.u, 16);
	o->exact_width = ft_strlen(o->content);
}

void					x_flags(t_arg *o)
{
	if (o->bitmap & HASH && (!(o->bitmap & ZERO) || \
		o->bitmap & MINUS) && !(o->bitmap & PRECISION && \
		o->precision_dt == 0 && o->container.d == 0) && \
		!(o->container.u == 0 && !(o->bitmap & PTR)))
		append_ox_simple(o);
}

void					x_width(t_arg *o)
{
	if (o->bitmap & WIDTH && o->width_dt > o->exact_width)
	{
		if (o->bitmap & MINUS)
			d_width_modify_minus(o);
		else if (o->bitmap & ZERO && (!(o->bitmap & PRECISION)))
		{
			d_width_modify_zero(o);
			if (o->bitmap & HASH)
				append_ox_zerowidth(o);
		}
		else
			d_width_modify_simple(o);
	}
}

void					x_vtbl(t_arg *o)
{
	o->type = &x_type;
	o->precision = &u_precision;
	o->flags = x_flags;
	o->width = &x_width;
}
