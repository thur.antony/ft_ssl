/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prepare.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:13:21 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:13:23 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				prepare(const char *format, va_list *ap)
{
	int			f_i0;
	int			f_i1;
	int			len;
	t_arg		*o;

	f_i0 = 0;
	f_i1 = 0;
	len = 0;
	while (format[f_i1] != '\0')
	{
		if (format[f_i1] == '%')
		{
			len += print_format(format, f_i0, f_i1);
			f_i0 = f_i1;
			o = new_arg();
			len += prepare_arg(o, format, &f_i0, ap);
			f_i1 = f_i0;
			del_arg(o);
		}
		else
			f_i1++;
	}
	len += print_format(format, f_i0, f_i1);
	return (len);
}

int				print_format(const char *format, int i0, int i1)
{
	int			len;

	len = i1 - i0;
	return (write(1, &format[i0], len));
}
