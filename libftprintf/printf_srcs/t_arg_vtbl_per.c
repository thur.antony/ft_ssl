/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_per.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/27 14:46:19 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/27 14:46:21 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					per_type(t_arg *o, va_list *ap)
{
	UNUSED(ap);
	o->container.s = "%";
	o->initial_width = 1;
	o->exact_width = o->initial_width;
	o->content = o->container.s;
}

void					per_vtbl(t_arg *o)
{
	o->type = &per_type;
	o->precision = NULL;
	o->width = &s_width;
	o->flags = NULL;
}
