/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_ls_h.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:16:55 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:16:57 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "flags.h"

int						get_binary_size(int num)
{
	int					size;

	size = 0;
	while (num)
	{
		size += 1;
		num = num >> 1;
	}
	return (size);
}

int						get_char_num(int size)
{
	if (size <= 7)
		return (1);
	else if (size <= 11)
		return (2);
	else if (size <= 16)
		return (3);
	return (4);
}

void					content_realloc(t_arg *o, int new_len)
{
	char				*tmp;
	int					len;
	int					i;

	if (o->content == NULL)
		o->content = ft_strnew(new_len);
	else
	{
		len = ft_strlen(o->content);
		tmp = ft_strnew(len);
		i = -1;
		while (o->content[++i])
			tmp[i] = o->content[i];
		free(o->content);
		o->content = ft_strnew(new_len);
		i = -1;
		while (tmp[++i])
			o->content[i] = tmp[i];
		free(tmp);
	}
}

void					apply_mask_ls(t_arg *o, int i, int c_i, int num_size)
{
	if (num_size == 1)
		o->content[c_i] = (char)o->container.ls[i];
	else if (num_size == 2)
	{
		o->content[c_i] = ((o->container.ls[i] >> 6) | MASK_H_TWO);
		o->content[++c_i] = ((o->container.ls[i] & ACC_L_TWO) | MASK_L);
	}
	else if (num_size == 3)
	{
		o->content[c_i] = ((o->container.ls[i] >> 12) | MASK_H_TR);
		o->content[++c_i] = (((o->container.ls[i] & ACC_M_TR) >> 6) | MASK_L);
		o->content[++c_i] = ((o->container.ls[i] & ACC_L_TR) | MASK_L);
	}
	else
	{
		o->content[c_i] = ((o->container.ls[i] >> 18) | MASK_H_F);
		o->content[++c_i] = (((o->container.ls[i] & ACC_N_F) >> 12) | MASK_L);
		o->content[++c_i] = (((o->container.ls[i] & ACC_M_F) >> 6) | MASK_L);
		o->content[++c_i] = ((o->container.ls[i] & ACC_L_F) | MASK_L);
	}
}
