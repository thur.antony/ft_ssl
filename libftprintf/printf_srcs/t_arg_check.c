/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 19:19:08 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/12 19:19:10 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					flag_check(t_arg *o, const char *f, int *i)
{
	while (1)
	{
		if (f[*i] == '#')
			change_bitmap(o, HASH);
		else if (f[*i] == '0')
			change_bitmap(o, ZERO);
		else if (f[*i] == '-')
			change_bitmap(o, MINUS);
		else if (f[*i] == '+')
			change_bitmap(o, PLUS);
		else if (f[*i] == ' ')
			change_bitmap(o, SPACE);
		else
			return ;
		*i = *i + 1;
	}
}

void					width_check(t_arg *o, const char *f, int *i)
{
	if (f[*i] > '0' && f[*i] <= '9')
	{
		set_width(o, ft_atoi(&f[*i]));
		change_bitmap(o, WIDTH);
	}
	while (f[*i] >= '0' && f[*i] <= '9')
	{
		*i = *i + 1;
	}
}

void					precision_check(t_arg *o, const char *f, int *i)
{
	if (f[*i] == '.')
	{
		change_bitmap(o, PRECISION);
		if (f[*i + 1] >= '0' && f[*i + 1] <= '9')
		{
			set_precision(o, ft_atoi(&f[++*i]));
			while (f[*i] >= '0' && f[*i] <= '9')
				*i = *i + 1;
		}
		else
			*i = *i + 1;
	}
}

void					length_check(t_arg *o, const char *f, int *i)
{
	if (f[*i] == 'h')
		(f[*i + 1] == 'h') ? change_bitmap(o, HH) : change_bitmap(o, H);
	else if (f[*i] == 'l')
		(f[*i + 1] == 'l') ? change_bitmap(o, LL) : change_bitmap(o, L);
	else if (f[*i] == 'j')
	{
		change_bitmap(o, J);
		*i = *i + 1;
	}
	else if (f[*i] == 'z')
	{
		change_bitmap(o, Z);
		*i = *i + 1;
	}
	while (f[*i] == 'h' || f[*i] == 'l')
		*i = *i + 1;
}

int						type_check(t_arg *o, const char *f, int *i)
{
	if (f[*i] == 'd' || f[*i] == 'i' || f[*i] == 'D')
		(f[*i] > 99) ? change_bitmap(o, D) : change_bitmap(o, LD);
	else if (f[*i] == 's' || f[*i] == 'S')
		(f[*i] == 's') ? change_bitmap(o, S) : change_bitmap(o, WS);
	else if (f[*i] == 'c' || f[*i] == 'C')
		(f[*i] == 'c') ? change_bitmap(o, C) : change_bitmap(o, WC);
	else if (f[*i] == 'o' || f[*i] == 'O')
		(f[*i] == 'o') ? change_bitmap(o, O) : change_bitmap(o, LO);
	else if (f[*i] == 'u' || f[*i] == 'U')
		(f[*i] == 'u') ? change_bitmap(o, U) : change_bitmap(o, LU);
	else if (f[*i] == 'x' || f[*i] == 'X')
		(f[*i] == 'x') ? change_bitmap(o, X) : change_bitmap(o, X_UP);
	else if (f[*i] == 'p')
		change_bitmap(o, PTR);
	else if (f[*i] == 'b')
		change_bitmap(o, B);
	else if (f[*i] == '%')
		change_bitmap(o, PER);
	else
		return (0);
	*i = *i + 1;
	set_i1(o, *i);
	return (1);
}
