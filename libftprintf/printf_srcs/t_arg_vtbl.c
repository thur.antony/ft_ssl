/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:14:53 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:14:54 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					create_vtbl(t_arg *o)
{
	if (o->bitmap & S)
		o->bitmap & L ? ls_vtbl(o) : s_vtbl(o);
	else if (o->bitmap & C)
		o->bitmap & L ? lc_vtbl(o) : c_vtbl(o);
	else if (o->bitmap & D)
		d_vtbl(o);
	else if (o->bitmap & U)
		u_vtbl(o);
	else if (o->bitmap & O)
		o_vtbl(o);
	else if (o->bitmap & X)
		x_vtbl(o);
	else if (o->bitmap & PTR)
		p_vtbl(o);
	else if (o->bitmap & PER)
		per_vtbl(o);
	else if (o->bitmap & B)
		b_vtbl(o);
}
