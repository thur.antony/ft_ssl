/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_content.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:14:32 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:14:34 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					create_content(t_arg *o, va_list *ap)
{
	if (o->type)
		o->type(o, ap);
	if (o->precision)
		o->precision(o);
	if (o->flags)
		o->flags(o);
	if (o->width)
		o->width(o);
	write(1, o->content, o->exact_width);
}
