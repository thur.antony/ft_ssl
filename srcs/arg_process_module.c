/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arg_process_module.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/18 17:54:45 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/18 17:54:47 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void						arg_process_hash_module(t_ssl_obj *o, int *i)
{
	if (!ft_strcmp(o->argv[*i], "-p"))
	{
		o->bitmap = o->bitmap | SSL_OPT_P;
		process_stdin(o);
	}
	else if (!ft_strcmp(o->argv[*i], "-q"))
	{
		o->bitmap = o->bitmap | SSL_OPT_Q;
		if (*i + 1 == o->i_arg_max)
			process_stdin(o);
	}
	else if (!ft_strcmp(o->argv[*i], "-r"))
	{
		o->bitmap = o->bitmap | SSL_OPT_R;
		if (*i + 1 == o->i_arg_max)
			process_stdin(o);
	}
	else if (!ft_strcmp(o->argv[*i], "-s") && *i + 1 < o->i_arg_max)
	{
		*i += 1;
		process_string(o, *i);
	}
	else
		process_file(o, *i);
}
