/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ssl_obj_content_out.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/18 21:03:37 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/18 21:03:38 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void						process_string(t_ssl_obj *o, int i)
{
	o->content = o->argv[i];
	o->content_length = ft_strlen(o->content);
	if (o->algorithm[o->i_algo])
		o->algorithm[o->i_algo](o);
	if (o->bitmap & SSL_OPT_Q)
		ft_printf("%s\n", o->output);
	else if (o->bitmap & SSL_OPT_R)
		ft_printf("%s \"%s\"\n", o->output, o->content);
	else
	{
		ft_printf("%s (\"%s\") = %s\n", o->algo_name, o->content, o->output);
	}
	free(o->output);
}

void						process_stdin(t_ssl_obj *o)
{
	o->stdin++;
	if (o->stdin == 1)
		o->content_length = get_text(0, &o->content);
	else
	{
		o->content_length = 0;
		o->content = "";
	}
	if (o->algorithm[o->i_algo])
		o->algorithm[o->i_algo](o);
	if (o->bitmap & SSL_OPT_P && o->stdin == 1)
		ft_printf("%s%s\n", o->content, o->output);
	else
		ft_printf("%s\n", o->output);
	if (o->stdin == 1)
		free(o->content);
	free(o->output);
}

void						process_file(t_ssl_obj *o, int i)
{
	int						fd;

	o->file_name = o->argv[i];
	if (opendir(o->file_name) != NULL)
	{
		o->error[DIR](o);
		return ;
	}
	if ((fd = open(o->file_name, O_RDONLY)) <= 0)
	{
		o->error[BAD_FILE](o);
		return ;
	}
	o->content_length = get_text(fd, &o->content);
	if (o->algorithm[o->i_algo])
		o->algorithm[o->i_algo](o);
	if (o->bitmap & SSL_OPT_Q)
		ft_printf("%s\n", o->output);
	else if (o->bitmap & SSL_OPT_R)
		ft_printf("%s %s\n", o->output, o->file_name);
	else
		ft_printf("%s (%s) = %s\n", o->algo_name, o->file_name, o->output);
	free(o->output);
	free(o->content);
}

static void					arg_process_hash_module_i(t_ssl_obj *o, int *i)
{
	if (!ft_strcmp(o->argv[*i], "-q"))
		o->bitmap = o->bitmap | SSL_OPT_Q;
	else if (!ft_strcmp(o->argv[*i], "-r"))
		o->bitmap = o->bitmap | SSL_OPT_R;
	else if (!ft_strcmp(o->argv[*i], "-s") && o->argv[*i + 1] != 0)
	{
		*i += 1;
		process_string(o, *i);
	}
	else
		process_file(o, *i);
}


void						interactive_mode_on(t_ssl_obj *o)
{
	char					*line;
	char					**words;
	int						i;

	while (1)
	{
		get_next_line(0, &line);
		if (!ft_strcmp(line, "exit"))
		{
			free(line);
			return ;
		}
		if (!(words = ft_strsplit(line, ' ')))
			o->error[BAD_MALLOC](o);
		ssl_enable_algo(o, words[0]);
		o->argv = words;
		i = 0;
		while (o->argv[++i] != 0)
			arg_process_hash_module_i(o, &i);
		i = -1;
		while (o->argv[++i] != 0)
			free(o->argv[i]);
		free(o->argv[i]);
		free(o->argv);
	}
}
