/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 16:55:24 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/18 21:09:31 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void						err_zero(t_ssl_obj *o)
{
	UNUSED(o);
	ft_printf("error: bad algorithm name\n");
	usage();
	exit(1);
}

void						err_one(t_ssl_obj *o)
{
	UNUSED(o);
	ft_printf("error: bad file name\n");
	o->file_name = NULL;
}

void						err_two(t_ssl_obj *o)
{
	UNUSED(o);
	ft_printf("error: it's a directory\n");
	o->file_name = NULL;
}

void						err_tree(t_ssl_obj *o)
{
	UNUSED(o);
	ft_printf("internal error: bad malloc");
	exit (1);
}

void						usage(void)
{
	char					*tmp;
	int						fd;

	fd = open("usage", O_RDONLY);
	get_text(fd, &tmp);
	ft_printf("%s\n", tmp);
	free(tmp);
	close(fd);
}
