/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ssl_obj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 16:55:31 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/18 21:10:11 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void						ssl_obj_init(t_ssl_obj *o, int argc, char **argv)
{
	o->bitmap = 0;
	o->i_algo = 0;
	o->i_arg_max = argc;
	o->argv = argv;
	o->stdin = 0;
	o->content = NULL;
	o->file_name = NULL;
	o->algo_name = NULL;
	o->output = NULL;
	ssl_obj_arg_process(o);
	ssl_obj_dispatcher(o);
	ssl_obj_error(o);
}

void						ssl_obj_arg_process(t_ssl_obj *o)
{
	o->arg_process[0] = NULL;
	o->arg_process[1] = arg_process_hash_module;
	o->arg_process[2] = arg_process_hash_module;
}

void						ssl_obj_dispatcher(t_ssl_obj *o)
{
	o->algorithm[0] = NULL;
	o->algorithm[1] = algo_md5;
	o->algorithm[2] = algo_sha256;
}

void						ssl_obj_error(t_ssl_obj *o)
{
	o->error[BAD_ALGO_OPT] = err_zero;
	o->error[BAD_FILE] = err_one;
	o->error[DIR] = err_two;
	o->error[BAD_MALLOC] = err_tree;
}

void						ssl_enable_algo(t_ssl_obj *o, char *s)
{
	if (!ft_strcmp("md5", s))
	{
		o->i_algo = MD5;
		o->algo_name = "MD5";
	}
	else if (!ft_strcmp("sha256", s))
	{
		o->i_algo = SHA256;
		o->algo_name = "SHA256";
	}
	else
		o->error[BAD_ALGO_OPT](o);
}
