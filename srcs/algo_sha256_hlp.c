/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_sha256_hlp.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/16 12:37:57 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/16 12:37:59 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

static const t_32	g_k_sha256[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1,
	0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
	0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786,
	0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
	0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
	0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
	0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
	0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
	0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

void						sha256_shedule_arr(t_sha256 *sha)
{
	int						i;

	i = -1;
	while (++i < 16)
		sha->m[i] = sha->word[i];
	while (i < 64)
	{
		sha->m[i] = SSM_1(sha->m[i - 2]) + \
					sha->m[i - 7] + \
					SSM_0(sha->m[i - 15]) + \
					sha->m[i - 16];
		i++;
	}
}

void						sha256_vals(t_sha256 *sha, int opt)
{
	if (opt == ASSIGN_SHA256)
	{
		sha->a = sha->hh[0];
		sha->b = sha->hh[1];
		sha->c = sha->hh[2];
		sha->d = sha->hh[3];
		sha->e = sha->hh[4];
		sha->f = sha->hh[5];
		sha->g = sha->hh[6];
		sha->h = sha->hh[7];
	}
	else if (opt == INCRM_SHA256)
	{
		sha->hh[0] += sha->a;
		sha->hh[1] += sha->b;
		sha->hh[2] += sha->c;
		sha->hh[3] += sha->d;
		sha->hh[4] += sha->e;
		sha->hh[5] += sha->f;
		sha->hh[6] += sha->g;
		sha->hh[7] += sha->h;
	}
}

void						sha256_inner_loop(t_sha256 *sha)
{
	int						i;

	i = -1;
	while (++i < 64)
	{
		sha->t1 = sha->h + SIGMA_1(sha->e) + \
				CH(sha->e, sha->f, sha->g) + \
				g_k_sha256[i] + sha->m[i];
		sha->t2 = SIGMA_0(sha->a) + MAJ(sha->a, sha->b, sha->c);
		sha->h = sha->g;
		sha->g = sha->f;
		sha->f = sha->e;
		sha->e = sha->d + sha->t1;
		sha->d = sha->c;
		sha->c = sha->b;
		sha->b = sha->a;
		sha->a = sha->t1 + sha->t2;
	}
}

void						sha256_add_precision(char **tmp)
{
	int						i;
	int						i1;
	char					*zero_str;
	char					*res;

	i = ft_strlen(*tmp);
	i1 = 8 - i;
	if (i1 > 0)
	{
		zero_str = ft_strnew_mod(i1, '0');
		res = ft_str_concat_d(zero_str, 1, *tmp, 1);
		*tmp = res;
	}
}

void						sha256_format_output(t_sha256 *sha, t_ssl_obj *o)
{
	char					*tmp;
	int						i;
	int						m;
	int						n;

	o->output = ft_strnew(64);
	i = 0;
	m = 0;
	while (i < 64 && m < 8)
	{
		tmp = ft_itoa_base_u_low(sha->hh[m], 16);
		sha256_add_precision(&tmp);
		n = 0;
		while (n < 8)
		{
			o->output[i] = tmp[n];
			i++;
			n++;
		}
		free(tmp);
		m++;
	}
}
