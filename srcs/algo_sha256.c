/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_sha256.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/16 12:37:52 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/16 12:37:53 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

static const t_32	g_h0_sha256 = 0x6a09e667;
static const t_32	g_h1_sha256 = 0xbb67ae85;
static const t_32	g_h2_sha256 = 0x3c6ef372;
static const t_32	g_h3_sha256 = 0xa54ff53a;
static const t_32	g_h4_sha256 = 0x510e527f;
static const t_32	g_h5_sha256 = 0x9b05688c;
static const t_32	g_h6_sha256 = 0x1f83d9ab;
static const t_32	g_h7_sha256 = 0x5be0cd19;

static void					msg_init(t_ssl_obj *o, t_sha256 *sha)
{
	t_32					bits_len;

	sha->len = o->content_length * 8 + 1;
	while (sha->len % 512 != 448)
		sha->len++;
	sha->len /= 8;
	sha->msg = (t_8 *)ft_strnew(sha->len + 64);
	ft_memcpy(sha->msg, o->content, o->content_length);
	sha->msg[o->content_length] = 128;
	bits_len = o->content_length * 8;
	sha->msg[sha->len + 4] = bits_len;
	sha->hh[0] = g_h0_sha256;
	sha->hh[1] = g_h1_sha256;
	sha->hh[2] = g_h2_sha256;
	sha->hh[3] = g_h3_sha256;
	sha->hh[4] = g_h4_sha256;
	sha->hh[5] = g_h5_sha256;
	sha->hh[6] = g_h6_sha256;
	sha->hh[7] = g_h7_sha256;
	sha->offset = 0;
}

static void					sha256_words_rotate(t_ssl_obj *o, t_sha256 *sha)
{
	int						i0;
	int						i1;
	t_32					temp;
	t_32					res;

	i1 = 0;
	i0 = o->content_length / 4 + 1;
	while (i1 < i0 && i1 < 16)
	{
		res = 0;
		temp = sha->word[i1];
		res = temp % 256;
		temp = temp / 256;
		res = res * 256 + temp % 256;
		temp = temp / 256;
		res = res * 256 + temp % 256;
		temp = temp / 256;
		res = res * 256 + temp % 256;
		temp = temp / 256;
		sha->word[i1] = res;
		i1++;
	}
}

void						algo_sha256(t_ssl_obj *o)
{
	t_sha256				sha;

	msg_init(o, &sha);
	while (sha.offset < sha.len)
	{
		sha.word = (t_32 *)(sha.msg + sha.offset);
		sha256_words_rotate(o, &sha);
		sha256_shedule_arr(&sha);
		sha256_vals(&sha, ASSIGN_SHA256);
		sha256_inner_loop(&sha);
		sha256_vals(&sha, INCRM_SHA256);
		sha.offset += (512 / 8);
	}
	sha256_format_output(&sha, o);
	free(sha.msg);
}
