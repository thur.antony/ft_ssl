/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 16:55:18 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/08 16:55:20 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

int						main(int argc, char **argv)
{
	t_ssl_obj			o;
	int					i;

	i = 1;
	ssl_obj_init(&o, argc, argv);
	i < argc ? ssl_enable_algo(&o, argv[i]) : interactive_mode_on(&o);
	if (i + 1 == argc)
		process_stdin(&o);
	while (++i < argc)
		o.arg_process[o.i_algo](&o, &i);
	return (0);
}
