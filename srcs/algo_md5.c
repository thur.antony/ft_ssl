/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_md5.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 18:37:22 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/14 18:37:24 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

static const t_32	g_h0_md5 = 0x67452301;
static const t_32	g_h1_md5 = 0xefcdab89;
static const t_32	g_h2_md5 = 0x98badcfe;
static const t_32	g_h3_md5 = 0x10325476;

static const t_32	g_r[] = {
	7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
	5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
	4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
	6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
};

static const t_32	g_k_md5[] = {
	0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
	0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
	0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
	0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
	0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
	0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
	0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
	0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
	0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
	0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
	0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
	0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
	0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
	0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
};

static void					msg_init(t_ssl_obj *o, t_md5 *md5)
{
	size_t					bits_len;

	md5->len = o->content_length * 8 + 1;
	while (md5->len % 512 != 448)
		md5->len++;
	md5->len /= 8;
	md5->msg = (t_8 *)ft_strnew(md5->len + 64);
	ft_memcpy(md5->msg, o->content, o->content_length);
	md5->msg[o->content_length] = 128;
	bits_len = 8 * o->content_length;
	ft_memcpy(md5->msg + md5->len, &bits_len, 8);
	md5->h[0] = g_h0_md5;
	md5->h[1] = g_h1_md5;
	md5->h[2] = g_h2_md5;
	md5->h[3] = g_h3_md5;
	md5->offset = 0;
}

static void					inner_loop(t_md5 *md5)
{
	if (md5->i < 16)
	{
		md5->f = (md5->b & md5->c) | ((~md5->b) & md5->d);
		md5->g = md5->i;
	}
	else if (md5->i < 32)
	{
		md5->f = (md5->d & md5->b) | ((~md5->d) & md5->c);
		md5->g = (5 * md5->i + 1) % 16;
	}
	else if (md5->i < 48)
	{
		md5->f = md5->b ^ md5->c ^ md5->d;
		md5->g = (3 * md5->i + 5) % 16;
	}
	else
	{
		md5->f = md5->c ^ (md5->b | (~md5->d));
		md5->g = (7 * md5->i) % 16;
	}
	md5->temp = md5->d;
	md5->d = md5->c;
	md5->c = md5->b;
	md5->b = md5->b + \
	ROT_L((md5->a + md5->f + g_k_md5[md5->i] + md5->word[md5->g]), g_r[md5->i]);
}

static void					md5_add_precision(char **tmp)
{
	int						i;
	int						i1;
	char					*zero_str;
	char					*res;

	i = ft_strlen(*tmp);
	i1 = 8 - i;
	if (i1 > 0)
	{
		zero_str = ft_strnew_mod(i1, '0');
		res = ft_str_concat_d(zero_str, 1, *tmp, 1);
		*tmp = res;
	}
}

static void					format_output(t_ssl_obj *o, t_md5 *md5)
{
	char					*tmp;
	int						i;
	int						j;
	int						m;

	o->output = ft_strnew_mod(32, '<');
	i = -1;
	m = 0;
	while (i < 32 && m <= 3)
	{
		j = 6;
		tmp = ft_itoa_base_u_low(md5->h[m], 16);
		md5_add_precision(&tmp);
		while (j >= 0)
		{
			o->output[++i] = tmp[j];
			o->output[++i] = tmp[j + 1];
			j -= 2;
		}
		free(tmp);
		m++;
	}
}

void						algo_md5(t_ssl_obj *o)
{
	t_md5					md5;

	msg_init(o, &md5);
	while (md5.offset < md5.len)
	{
		md5.word = (t_32 *)(md5.msg + md5.offset);
		md5.a = md5.h[0];
		md5.b = md5.h[1];
		md5.c = md5.h[2];
		md5.d = md5.h[3];
		md5.i = 0;
		while (md5.i < 64)
		{
			inner_loop(&md5);
			md5.a = md5.temp;
			md5.i++;
		}
		md5.h[0] += md5.a;
		md5.h[1] += md5.b;
		md5.h[2] += md5.c;
		md5.h[3] += md5.d;
		md5.offset += (512 / 8);
	}
	format_output(o, &md5);
	free(md5.msg);
}
